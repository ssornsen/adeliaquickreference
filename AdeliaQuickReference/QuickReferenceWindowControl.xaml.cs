﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Controls.Primitives;

namespace AdeliaQuickReference
{
    /// <summary>
    /// Interaction logic for QuickReferenceWindowControl.
    /// </summary>
    public partial class QuickReferenceWindowControl : UserControl
    {
        private ObservableCollection<AdeliaColumn> adeliaColumns { get; set; }
        private ObservableCollection<AdeliaColumn> displayedColumns { get; set; }

        public void SetSearchedText(string text)
        {
            TextBox_Search.Text = text;
            TextBox_Filter.Text = "";
            SearchButton.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickReferenceWindowControl"/> class.
        /// </summary>
        public QuickReferenceWindowControl()
        {
            this.InitializeComponent();
            adeliaColumns = new ObservableCollection<AdeliaColumn>();
            ListBox_AdeliaColumns.ItemsSource = adeliaColumns;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            TextBox_Filter.Text = "";
            var adh = new AdeliaSearch();

            try
            {
                IEnumerable<AdeliaColumn> searchedCols = adh.GetColumns(TextBox_Search.Text.ToUpper())
                                                            .Where(r => r.IsVirtual == false);
                AdeliaTable searchedTable = adh.GetTable(TextBox_Search.Text.ToUpper());
                adeliaColumns = new ObservableCollection<AdeliaColumn>(searchedCols);
                ListBox_AdeliaColumns.ItemsSource = adeliaColumns;

                TextBlock_Description.Text = searchedTable.Description;
            }
            catch (Exception ex) 
            {
            }
        }

        private void PhysicalNameCM_Click(object sender, RoutedEventArgs e)
        {
            //maybe open in browser
        }

        private void TextBox_Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(TextBox_Filter.Text))
            {
                var filtredCols = adeliaColumns.Where(i => i.PhysicalName.Contains(TextBox_Filter.Text));
                displayedColumns = new ObservableCollection<AdeliaColumn>(filtredCols);
                ListBox_AdeliaColumns.ItemsSource = displayedColumns;
            }
            else
            {
                ListBox_AdeliaColumns.ItemsSource = adeliaColumns;
            }
        }
    }
}