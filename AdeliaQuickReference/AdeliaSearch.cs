﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace AdeliaQuickReference
{
    public class AdeliaSearch
    {
        public AdeliaSearch() { }

        public AdeliaTable GetTable(string table, string context = "all914c")
        {
            if (!IsCorrectAdeliaTableFormat(table))
            {
                return null;
            }

            string url = $"http://sgedreflex5/api/v1.0/{context}/tables/sections/{table.Substring(0, 2)}?search=&sort=physicalName&order=asc";
            IEnumerable<AdeliaTable> tables = Get<IEnumerable<AdeliaTable>>(url, context);
            return tables.FirstOrDefault(e => e.PhysicalName == table);
        }

        public IEnumerable<AdeliaColumn> GetColumns(string table, string context = "all914c")
        {
            if (!IsCorrectAdeliaTableFormat(table))
            {
                return null;
            }

            string url = $"http://sgedreflex5/api/v1.0/{context}/tables/physical/{table}/columns?search=&sort=index&order=asc";
            return Get<IEnumerable<AdeliaColumn>>(url, context);
        }

        public T Get<T>(string url, string context = "all914c")
        {
            var client = new HttpClient(new HttpClientHandler()
            {
                UseDefaultCredentials = true,
            });

            var result = client.GetAsync(url).Result;
            string reqResult = result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<T>(reqResult);
        }

        private bool IsCorrectAdeliaTableFormat(string tableName)
        {
            if (!tableName.All(c => char.IsLetter(c))
             || string.IsNullOrWhiteSpace(tableName))
                return false;
            else
                return true;

        }
    }
}
