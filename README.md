Visual Studio extension to quickly consult Adelia Columns

Installation folder is in bin/Release

To create a new Release :
-   Increase the version in the source.extension.vsixmanifest file (this allows to update the extension without having to uninstall it)
-   Compile the new version in Release
-   Commit the Release folder
-   Create a new tag
-   Create a new release off this tag and point to the release folder in the resources

