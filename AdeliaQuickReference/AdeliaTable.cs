﻿
using Newtonsoft.Json;

namespace AdeliaQuickReference
{
    public class AdeliaTable
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("logicalName")]
        public string LogicalName { get; set; }
        [JsonProperty("physicalName")]
        public string PhysicalName { get; set; }
        [JsonProperty("section")]
        public string Section { get; set; }
    }
}
