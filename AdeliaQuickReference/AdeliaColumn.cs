﻿using Newtonsoft.Json;

namespace AdeliaQuickReference
{
    //pas complet
    public class AdeliaColumn
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("index")]
        public int Index { get; set; }
        [JsonProperty("physicalName")]
        public string PhysicalName { get; set; }
        [JsonProperty("logicalName")]
        public string LogicalName { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("columnType")]
        public string Type { get; set; }
        [JsonProperty("primaryKeyIndex")]
        public int? PrimaryKeyIndex { get; set; }
        [JsonProperty("isPrimaryKey")]
        public bool Primary { get; set; }
        [JsonProperty("isVirtual")]
        public bool IsVirtual { get; set; }
        [JsonProperty("precision")]
        public decimal Precision { get; set; }
    }
}
